package org.example

import javafx.geometry.Orientation
import javafx.scene.chart.NumberAxis
import org.controlsfx.control.table.TableFilter
import org.nield.kotlinstatistics.multiKMeansCluster
import tornadofx.*
import java.time.LocalDate
import java.time.temporal.ChronoUnit

class MyApp : App(MyView::class)

class MyView : View() {

    override val root = splitpane {
        orientation = Orientation.HORIZONTAL

        tableview<Patient> {

            readonlyColumn("FIRSTNAME", Patient::firstName)
            readonlyColumn("LASTNAME", Patient::lastName)
            readonlyColumn("GENDER", Patient::gender)
            readonlyColumn("BIRTHDAY", Patient::birthday)
            readonlyColumn("AGE", Patient::age)
            readonlyColumn("WBCC", Patient::count)


            items.setAll(patients)
//            TableFilter.forTableView(this).apply()
        }


        scatterchart("WBCC cluster by age", NumberAxis(), NumberAxis()) {
            patients.multiKMeansCluster(k = 3,
                maxIterations = 10000,
                trialCount = 50,
                xSelector = { it.age.toDouble() },
                ySelector = { it.count.toDouble() })
                .forEachIndexed { index, centroid ->
                    series("Group ${index + 1}") {
                        centroid.points.forEach {
                            println("\t$it")
                            data(it.age, it.count)
                        }
                    }
                }
        }
    }
}

enum class Gender {
    MALE, FEMALE
}

data class Patient(
    val firstName: String,
    val lastName: String,
    val gender: Gender,
    val birthday: LocalDate,
    val count: Int
) {
    val age = ChronoUnit.YEARS.between(birthday, LocalDate.now())
}

val patients = listOf(
    Patient("Jon", "Smith", Gender.MALE, LocalDate.of(1987, 11, 1), 2300),
    Patient("John", "Doe", Gender.MALE, LocalDate.of(1990, 6, 1), 3400),
    Patient("Nate", "Snow", Gender.MALE, LocalDate.of(2000, 11, 25), 4500),
    Patient("Queen", "Gambit", Gender.FEMALE, LocalDate.of(1985, 11, 19), 6300),
    Patient("Brian", "Newman", Gender.MALE, LocalDate.of(1997, 5, 12), 8300),
    Patient("Eric", "Wright", Gender.MALE, LocalDate.of(1993, 4, 7), 1300),
    Patient("Jessica", "Norway", Gender.FEMALE, LocalDate.of(1999, 6, 7), 1300),
    Patient("Jane", "Doe", Gender.FEMALE, LocalDate.of(2001, 5, 9), 1300),
    Patient("John", "Simone", Gender.MALE, LocalDate.of(1989, 1, 7), 4500),
    Patient("Sarah", "Marley", Gender.FEMALE, LocalDate.of(1970, 2, 5), 6700),
    Patient("Jessica", "Arnold", Gender.FEMALE, LocalDate.of(1980, 3, 9), 3400),
    Patient("Sam", "Beasley", Gender.MALE, LocalDate.of(1981, 4, 17), 8800),
    Patient("Dan", "Forney", Gender.MALE, LocalDate.of(1985, 9, 13), 5400),
    Patient("Lauren", "Michaels", Gender.FEMALE, LocalDate.of(1975, 8, 21), 5000),
    Patient("Michael", "Erlich", Gender.MALE, LocalDate.of(1985, 12, 17), 4100),
    Patient("Jason", "Miles", Gender.MALE, LocalDate.of(1991, 11, 1), 3900),
    Patient("Rebekah", "Earley", Gender.FEMALE, LocalDate.of(1985, 2, 18), 4600),
    Patient("James", "Larson", Gender.MALE, LocalDate.of(1974, 4, 10), 5100),
    Patient("Dan", "Ulrech", Gender.MALE, LocalDate.of(1991, 7, 11), 6000),
    Patient("Heather", "Eisner", Gender.FEMALE, LocalDate.of(1994, 3, 6), 6000),
    Patient("Jasper", "Martin", Gender.MALE, LocalDate.of(1971, 7, 1), 6000)


)

//configuration
//--module-path /Users/jkivu/Downloads/openjfx-11.0.2_windows-x64_bin-sdk/javafx-sdk-11.0.2/lib --add-modules=javafx.controls,javafx.fxml