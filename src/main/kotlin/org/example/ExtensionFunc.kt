package org.example

import org.nield.kotlinstatistics.percentileBy

fun main(args: Array<String>) {

    fun Collection<Patient>.wbccPercentileByGender(percentile: Double) =
        percentileBy(
            percentile = percentile,
            keySelector = { it.gender },
            valueSelector = { it.count }
        )

    val percentileQuadrantsByGender = patients.let {
        mapOf(1.0 to it.wbccPercentileByGender(1.0),
            25.0 to it.wbccPercentileByGender(25.0),
            50.0 to it.wbccPercentileByGender(50.0),
            75.0 to it.wbccPercentileByGender(75.0),
            100.0 to it.wbccPercentileByGender(100.0)
        )
    }

    percentileQuadrantsByGender.forEach(::println)
}